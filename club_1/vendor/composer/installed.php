<?php return array (
  'root' => 
  array (
    'pretty_version' => '1.0.0+no-version-set',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => '__root__',
  ),
  'versions' => 
  array (
    '__root__' => 
    array (
      'pretty_version' => '1.0.0+no-version-set',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'cknow/laravel-money' => 
    array (
      'pretty_version' => 'v6.1.1',
      'version' => '6.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c1e9a74a58296e4844e3b247521219b4b16a7932',
    ),
    'doctrine/inflector' => 
    array (
      'pretty_version' => '2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '9cf661f4eb38f7c881cac67c75ea9b00bf97b210',
    ),
    'illuminate/bus' => 
    array (
      'pretty_version' => 'v8.61.0',
      'version' => '8.61.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '808097c0dfd893309bd77b00139586c516b965c9',
    ),
    'illuminate/collections' => 
    array (
      'pretty_version' => 'v8.61.0',
      'version' => '8.61.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '18fa841df912ec56849351dd6ca8928e8a98b69d',
    ),
    'illuminate/container' => 
    array (
      'pretty_version' => 'v8.61.0',
      'version' => '8.61.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '862b64ea4ab56e307a1676104a1b93295d347ad0',
    ),
    'illuminate/contracts' => 
    array (
      'pretty_version' => 'v8.61.0',
      'version' => '8.61.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ab4bb4ec3b36905ccf972c84f9aaa2bdd1153913',
    ),
    'illuminate/events' => 
    array (
      'pretty_version' => 'v8.61.0',
      'version' => '8.61.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7def78033f29cd0c0383513b27c291d233a7f90e',
    ),
    'illuminate/filesystem' => 
    array (
      'pretty_version' => 'v8.61.0',
      'version' => '8.61.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f33219e5550f8f280169e933b91a95250920de06',
    ),
    'illuminate/macroable' => 
    array (
      'pretty_version' => 'v8.61.0',
      'version' => '8.61.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '300aa13c086f25116b5f3cde3ca54ff5c822fb05',
    ),
    'illuminate/pipeline' => 
    array (
      'pretty_version' => 'v8.61.0',
      'version' => '8.61.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '23aeff5b26ae4aee3f370835c76bd0f4e93f71d2',
    ),
    'illuminate/support' => 
    array (
      'pretty_version' => 'v8.61.0',
      'version' => '8.61.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b0a21c41163381dd9a5abbd68fe85ed7b4247d30',
    ),
    'illuminate/view' => 
    array (
      'pretty_version' => 'v8.61.0',
      'version' => '8.61.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f47b9261a4f275885d51bd389cb320f9f3b7a6f8',
    ),
    'moneyphp/money' => 
    array (
      'pretty_version' => 'v3.3.1',
      'version' => '3.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '122664c2621a95180a13c1ac81fea1d2ef20781e',
    ),
    'nesbot/carbon' => 
    array (
      'pretty_version' => '2.53.1',
      'version' => '2.53.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f4655858a784988f880c1b8c7feabbf02dfdf045',
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '8622567409010282b7aeebe4bb841fe98b58dcaf',
    ),
    'psr/container-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/simple-cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
    ),
    'stripe/stripe-php' => 
    array (
      'pretty_version' => 'v7.97.0',
      'version' => '7.97.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ae41c309ce113362706f8d5f19cf0cf2c730bc4a',
    ),
    'symfony/deprecation-contracts' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5f38c8804a9e97d23e0c8d63341088cd8a22d627',
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => 'v5.3.7',
      'version' => '5.3.7.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a10000ada1e600d109a6c7632e9ac42e8bf2fb93',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.23.1',
      'version' => '1.23.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '9174a3d80210dca8daa7f31fec659150bbeabfc6',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.23.1',
      'version' => '1.23.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '1100343ed1a92e3a38f9ae122fc0eb21602547be',
    ),
    'symfony/translation' => 
    array (
      'pretty_version' => 'v5.3.7',
      'version' => '5.3.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '4d595a6d15fd3a2c67f6f31d14d15d3b7356d7a6',
    ),
    'symfony/translation-contracts' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '95c812666f3e91db75385749fe219c5e494c7f95',
    ),
    'symfony/translation-implementation' => 
    array (
      'provided' => 
      array (
        0 => '2.3',
      ),
    ),
    'voku/portable-ascii' => 
    array (
      'pretty_version' => '1.5.6',
      'version' => '1.5.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '80953678b19901e5165c56752d087fc11526017c',
    ),
  ),
);
